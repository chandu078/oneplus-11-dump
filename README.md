# qssi-user 13 TP1A.220905.001 1675265110324 release-keys
- manufacturer: oplus
- platform: kalama
- codename: ossi
- flavor: qssi-user
- release: 13
- id: TP1A.220905.001
- incremental: 1675265110324
- tags: release-keys
- fingerprint: oplus/ossi/ossi:13/TP1A.220905.001/1675170186363:user/release-keys
- is_ab: true
- brand: oplus
- branch: qssi-user-13-TP1A.220905.001-1675265110324-release-keys
- repo: oplus_ossi_dump
